#ifndef MAGIC_H
#define MAGIC_H

#include <cstddef>

namespace magick {
    /// magick sum
    std::size_t sum(std::size_t a, std::size_t b);
}

#endif /* MAGIC_H */
