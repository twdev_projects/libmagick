# libmagick

This is an example project, demonstrating typical usage of [meson build system](https://mesonbuild.com/).

Please, refer back to a [blog article](https://twdev.blog/2022/09/meson/) for more details.
