#include <gtest/gtest.h>

#include <libmagick/magic.h>


class MagickTest : public ::testing::Test {
public:
    void SetUp() {
    }

    void TearDown() {
    }
};


TEST_F(MagickTest, test_ifNumbersAddUp) {
    ASSERT_EQ(5, magick::sum(2, 3));
}
